module.exports = {
  env: {
    PORT: 3002,
    localStorageKey: 'is_survey',
    userdataCookie: 'is_session',
    url: 'http://localhost:3002'
  },
}
