import React from 'react'

import styles from './HamBurgerButton.module.scss'

const HamBurgerButton = () => {
  return (
    <>
      <span className={styles.ham1} />
      <span className={styles.ham2} />
      <span className={styles.ham3} />
    </>
  )
}

export default HamBurgerButton
