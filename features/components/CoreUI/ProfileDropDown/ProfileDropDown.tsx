import React from 'react'

import styles from './ProfileDropDown.module.scss'
import FlexContainer from '../FlexContainer/FlexContainer'

const ProfileDropDown = () => {
  return (
    <FlexContainer justify='end' classlist={styles.profileDropWrapper}>
      <div className={styles.profileDropImgWrapper}>
        <span className={styles.profileStatus} />
        <img src={'/static/images/DefaultProfileImage.png'} alt='profileImage' />
      </div>
      <img src={'/static/icons/angle_down.svg'} />
    </FlexContainer>
  )
}

export default ProfileDropDown
