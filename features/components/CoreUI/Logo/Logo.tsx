import React, { FC } from 'react'

export const Logo: FC = () => (
  <img src='/static/icons/logo.svg' alt='Imagination Spectrum' />
)

export const LogoWhite: FC = () => (
  <img src='/static/icons/logo-white.svg' alt='Imagination Spectrum' />
)

