export const navLinks: string
export const navButton: string
export const navElementsWrapper: string
export const hamBurgerButton: string
export const light: string
export const dark: string
export const navSectionWrapper: string
