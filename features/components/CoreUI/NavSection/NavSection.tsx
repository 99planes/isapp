import React, { FC } from 'react'

import styles from './NavSection.module.scss'
import FlexContainer from '../FlexContainer/FlexContainer'
import Link from 'next/link'
import Button from '../Button/Button'
import HamBurgerButton from '../HamBurgerButton'

interface Props{
  setTheme: any
    theme: string
}

const NavSection: FC<Props> = ({ setTheme, theme }) => {
  return (
    <div className={styles.navSectionWrapper}>
      <span className={styles.hamBurgerButton}>
        <HamBurgerButton />
      </span>
      <FlexContainer align='center' classlist={styles.navElementsWrapper} >
        <div className={`${styles.navLinks}  ${styles[theme]}`} >
          <Link href={'/home'} >Home</Link>
          <Link href={'/home/profile'} >Profile</Link>
          <Link href={'#'}>Articles & Surveys</Link>
          <Link href={'#'}>Community</Link>
        </div>
        <div className={styles.navButton} onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}>
          <Button variant='gradient' type='button'  >Premium</Button>
        </div>
      </FlexContainer>
    </div>
  )
}

export default NavSection
