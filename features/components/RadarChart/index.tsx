import * as d3 from 'd3'
import { useEffect, useRef, useState } from 'react'
const data = [
  [// iPhone
    { name: 'Visual', color: 'FFC723', value: 90, isVisible: true  },
    { name: '',  color: '', value: 40 },
    { name: 'Olfactory', color: 'FFC723',  value: 60, isVisible: true },
    { name: '',  color: '', value: 40 },
    { name: 'Emotion', color: 'FF6442',  value: 70, isVisible: true },
    { name: '',  color: '', value: 40 },
    { name: 'Auditory', color: 'FF6442',  value: 70, isVisible: true },
    { name: '',  color: '', value: 40 },
    { name: 'Tactile', color: '6E1CEB',  value: 90, isVisible: true },
    { name: '',  color: '', value: 40 },
    { name: 'Gustatory', color: '6E1CEB',  value: 90, isVisible: true },
    { name: '',  color: '', value: 40 },
    { name: 'Motor', color: 'EA2A89',  value: 80, isVisible: true },
    { name: '',  color: '', value: 40 },
  ]
]

function debounce(fn, ms) {
  let timer
  return _ => {
    clearTimeout(timer)
    timer = setTimeout(_ => {
      timer = null
      // eslint-disable-next-line prefer-rest-params
      fn.apply(this, arguments)
    }, ms)
  }
}

const RadarChart = () => {
  const [dimensions, setDimensions] = useState({})
  const chartRef = useRef()
  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: chartRef.current.clientWidth
      })
    }, 100)

    window.addEventListener('resize', debouncedHandleResize)

    return () => window.removeEventListener('resize', debouncedHandleResize)
  })
  useEffect(() => {
    const id = '.radarChart'
    const margin  = { top: 100, right: 100, bottom: 100, left: 100 }
    const width   = Math.min(700, chartRef.current.clientWidth - 10) - margin.left - margin.right
    const height  = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20)
    const options = {
      w: width,
      h: height,
      margin: margin,
      maxValue: 0.5,
      levels: 5,
      roundStrokes: true,
      color: d3.scaleOrdinal()
        .range(['#EDC951', '#CC333F', '#00A0B0'])
    }
    const cfg = {
      w: 600,
      h: 600,
      margin: { top: 20, right: 20, bottom: 20, left: 20 },
      levels: 0,
      maxValue: 0,
      labelFactor: 1.25,
      wrapWidth: 60,
      opacityArea: 0.35,
      dotRadius: 4,
      opacityCircles: 0.02,
      opacityCirclesStroke: 0.1,
      strokeWidth: 15,
      roundStrokes: false,
      color: d3.scaleOrdinal(d3.schemeCategory10)
    }

    // Put all of the options into a variable called cfg
    if (typeof options !== 'undefined') {
      for (const i in options) {
        if (typeof options[i] !== 'undefined') { cfg[i] = options[i] }
      }
    }

    // If the supplied maxValue is smaller than the actual one, replace by the max in the data
    const maxValue = Math.max(cfg.maxValue, d3.max(data, function (i) {
      return d3.max(i.map(function (o) {
        return o.value
      }))
    }) as number)

    const allname = data[0].map(function(i, j) { return { name: i.name, color: i.color, score: i.value, isVisible: i.isVisible } })
    const total = allname.length
    const radius = Math.min(cfg.w / 2, cfg.h / 2)
    const angleSlice = Math.PI * 2 / total
    const rScale = d3.scaleLinear()
      .range([0, radius])
      .domain([0, maxValue])
    /// //////////////////////////////////////////////////////
    /// ///////// Create the container SVG and g /////////////
    /// //////////////////////////////////////////////////////

    // Remove whatever chart with the same id/class was present before
    d3.select(id).select('svg').remove()

    // Initiate the radar chart SVG
    const svg = d3.select(id).append('svg')
      .attr('width',  cfg.w + cfg.margin.left + cfg.margin.right)
      .attr('height', cfg.h + cfg.margin.top + cfg.margin.bottom)
      .attr('class', 'radar' + id)

    // Append a g element
    const g = svg.append('g')
      .attr('transform', 'translate(' + (cfg.w / 2 + cfg.margin.left) + ',' + (cfg.h / 2 + cfg.margin.top) + ')')

    // Set up the small tooltip for when you hover over a circle
    const tooltip = g.append('text')
      .attr('class', 'tooltip')
      .style('opacity', 0)

    /// //////////////////////////////////////////////////////
    /// /////// Glow filter for some extra pizzazz ///////////
    /// //////////////////////////////////////////////////////

    // Filter for the outside glow
    const filter = g.append('defs').append('filter').attr('id', 'glow')
    filter.append('feGaussianBlur').attr('stdDeviation', '2.5').attr('result', 'coloredBlur')
    const feMerge = filter.append('feMerge')
    feMerge.append('feMergeNode').attr('in', 'coloredBlur')
    feMerge.append('feMergeNode').attr('in', 'SourceGraphic')


    /// //////////////////////////////////////////////////////
    /// //////////// Draw the Circular grid //////////////////
    /// //////////////////////////////////////////////////////

    // Wrapper for the grid & axes
    const nameGrid = g.append('g').attr('class', 'nameWrapper')

    // Draw the background circles
    nameGrid.selectAll('.levels')
      .data([
        d3.range(1, (cfg.levels + 1)).reverse()[0],
        d3.range(1, (cfg.levels + 1)).reverse()[d3.range(1, (cfg.levels + 1)).reverse().length - 2]]
      )
      .enter()
      .append('circle')
      .attr('class', 'gridCircle')
      .attr('r', function(d, i) { return radius / cfg.levels * d })
      .style('fill', '#000000')
      .style('stroke', '#cdcdcd')
      .style('fill-opacity', cfg.opacityCircles)
      .style('stroke-opacity', cfg.opacityCirclesStroke)
      .style('filter', 'url(#glow)')

    /// //////////////////////////////////////////////////////
    /// ///////////////// Draw the axes //////////////////////
    /// //////////////////////////////////////////////////////

    // Create the straight lines radiating outward from the center
    const name = nameGrid.selectAll('.name')
      .data(allname)
      .enter()
      .append('g')
      .attr('class', 'name')

    nameGrid
      .append('text')
      .attr('x', -38)
      .attr('y', 15)
      .attr('id', 'score')
      .text('57')
      .style('font-size', '70px')
      .style('font-weight', 600)
      .style('fill', 'white')

    // Append the lines
    name.append('line')
      .attr('x1', 0)
      .attr('y1', 0)
      .attr('x2', function(d, i) { return rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2) })
      .attr('y2', function(d, i) { return rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2) })
      .attr('class', 'line')
      .style('stroke', 'gray')
      .style('stroke-width', '1px')
      .style('opacity', d => d.name ? 0.2 : 0)


    nameGrid
      .append('text')
      .attr('x', -40)
      .attr('y', 35)
      .text('Overall score')
      .style('font-size', '13px')
      .style('font-weight', 400)
      .style('fill', 'white')

    function wrap(text, width) {
      text.each(function() {
        const text = d3.select(this)
        const words = text.text().split(/\s+/).reverse()
        let word
        let line = []
        let lineNumber = 0
        const lineHeight = 1.4 // ems
        const y = text.attr('y')
        const x = text.attr('x')
        const dy = parseFloat(text.attr('dy'))
        let tspan = text.text(null).append('tspan').attr('x', x).attr('y', y).attr('dy', dy + 'em')

        while (word = words.pop()) {
          line.push(word)
          tspan.text(line.join(' '))
          if (tspan.node().getComputedTextLength() > width) {
            line.pop()
            tspan.text(line.join(' '))
            line = [word]
            tspan = text.append('tspan').attr('x', x).attr('y', y).attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word)
          }
        }
      })
    }// wrap

    name.append('circle')
      .attr('transform', function(d, i) {
        if (d.name) {
          const x = rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice * i - Math.PI / 2)
          const y = rScale(maxValue * cfg.labelFactor) * Math.sin(angleSlice * i - Math.PI / 2)
          return `translate(${x}, ${y})`
        }
      })
      .style('opacity', d => d.isVisible ? 1 : 0)
      .attr('cursor', 'pointer')
      .attr('dy', '1em')
      .attr('x', function(d, i) { return rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice * i - Math.PI / 2) })
      .attr('y', function(d, i) { return rScale(maxValue * cfg.labelFactor) * Math.sin(angleSlice * i - Math.PI / 2) })
      .attr('r', function(d) { return 30 })
      .attr('stroke', function(d) { return `#${d.color}` })
      .attr('fill', '#020F3D')
      .call(wrap, cfg.wrapWidth)
      .on('mouseover', function(d, i) {
        if (i.name) {
          d3.select('#score').remove()
          nameGrid
            .append('text')
            .attr('x', -38)
            .attr('y', 15)
            .attr('id', 'score')
            .text(i.score.toString())
            .style('font-size', '70px')
            .style('font-weight', 600)
            .style('fill', 'white')

          const currentX = parseFloat(d3.select(this).attr('x'))
          const newX =  currentX < -1 ? currentX - 60 : currentX + 30
          const newY =  parseFloat(d3.select(this).attr('y')) - 35

          tooltip
            .attr('x', newX)
            .attr('y', newY)
            .text(i.name)
            .transition()
            .duration(200)
            .style('opacity', 1)
        }
      })
      .on('mouseout', function() {
        tooltip.transition().duration(200)
          .style('opacity', 0)
      })
    // Append the labels at each name
    name.append('text')
      .attr('class', 'legend')
      .style('font-size', '11px')
      .style('fill', '#FFFFFF')
      .attr('text-anchor', 'middle')
      .attr('dy', '0.35em')
      .attr('x', function(d, i) { return rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice * i - Math.PI / 2) })
      .attr('y', function(d, i) { return rScale(maxValue * cfg.labelFactor) * Math.sin(angleSlice * i - Math.PI / 2) })
      // .text(function(d) { return d.name })
      .call(wrap, cfg.wrapWidth)

    /// //////////////////////////////////////////////////////
    /// ////////// Draw the radar chart blobs ////////////////
    /// //////////////////////////////////////////////////////

    // The radial line function
    const radarLine = d3.lineRadial()
      // .interpolate('linear-closed')
      .curve(d3.curveLinear)
      .radius(function(d) { return rScale(d.value) })
      .angle(function(d, i) {	return i * angleSlice })

    if (cfg.roundStrokes) {
      radarLine.curve(d3.curveCardinalClosed)
      // radarLine.interpolate('cardinal-closed')
    }

    // Create a wrapper for the blobs
    const blobWrapper = g.selectAll('.radarWrapper')
      .data(data)
      .enter().append('g')
      .attr('class', 'radarWrapper')

    const colorRange = ['#FFC723', '#FF6442', '#EA2A89', '#6E1CEB']
    const color = d3.scaleLinear().range(colorRange).domain([1, 2, 3, 4])
    const linearGradient = g.append('defs')
      .append('linearGradient')
      .attr('id', 'linear-gradient')
      .attr('gradientTransform', 'rotate(100)')

    linearGradient.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', color(1))

    linearGradient.append('stop')
      .attr('offset', '33.33%')
      .attr('stop-color', color(2))

    linearGradient.append('stop')
      .attr('offset', '66.66%')
      .attr('stop-color', color(3))

    linearGradient.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', color(4))

    // Create the outlines
    blobWrapper.append('path')
      .attr('class', 'radarStroke')
      .attr('d', function(d, i) { return radarLine(d) })
      .style('stroke-width', cfg.strokeWidth + 'px')
      .style('stroke', 'url(#linear-gradient)')
      .style('fill', 'none')
      .style('filter', 'url(#glow)')

    // Append the circles
    blobWrapper.selectAll('.radarCircle')
      .data(function(d, i) { return d })
      .enter()
      .append('circle')
      .attr('class', 'radarCircle')
      .attr('r', cfg.dotRadius)
      .attr('cx', function(d, i) { return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2) })
      .attr('cy', function(d, i) { return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2) })
      .style('fill', function(d, i, j) { return cfg.color(j) })
      .style('fill-opacity', x => x.name ? 0 : 0)
  })

  return (
    <div className='radarChart' ref={chartRef}></div>
  )
}
export default RadarChart
