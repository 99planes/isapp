import React, { FC } from 'react'

import styles from './ResultSummary.module.scss'
import Button from '../../../components/CoreUI/Button/Button'

interface Props{
    resultSummary: ResultSummary
}

interface ResultSummary {
    title : string,
    detail: string
}

const ResultSummary: FC<Props> = ({ resultSummary }) => {
  return (
    <div className={styles.homeRightWrapper}>
      <h5>Your result</h5>
      <h1>{resultSummary.title}</h1>
      <p>{resultSummary.detail}</p>
      <Button variant='purple' >Learn more</Button>
      <Button variant='inverse' className={styles.homeShareButton}><img src={'/static/icons/share-icon.svg'} />Share</Button>
    </div>)
}

export default ResultSummary
