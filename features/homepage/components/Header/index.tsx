import React, { FC } from 'react'
import Link from 'next/link'

import styles from './Header.module.scss'
import FlexContainer from '../../../components/CoreUI/FlexContainer/FlexContainer'
import { LogoWhite, Logo } from '../../../components/CoreUI'
import NavSection from '../../../components/CoreUI/NavSection/NavSection'
import ProfileDropDown from '../../../components/CoreUI/ProfileDropDown/ProfileDropDown'

interface Props{
  setTheme:any
  theme: string
}

const TempHeader: FC<Props> = ({ setTheme, theme }) => {
  return (
    <header>
      <FlexContainer align='center' direction='row' justify='start' classlist={styles.homeHeader}>
        <Link href={'/home'}>{theme === 'light' ? <Logo /> : <LogoWhite/>}</Link>
        <NavSection setTheme={setTheme} theme={theme} />
        <div className={styles.tempTaskLogos}>
          <img src={'/static/icons/search.svg'} alt='searchIcon' />
          <span>ENG</span>
        </div>
        <ProfileDropDown />
      </FlexContainer>
    </header>

  )
}

export default TempHeader
