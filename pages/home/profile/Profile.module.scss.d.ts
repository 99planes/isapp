export const profileContainer: string
export const light: string
export const dark: string
export const profileHeader: string
export const profileLeftWrapper: string
export const profileCoverWrapper: string
