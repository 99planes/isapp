import React, { FC, useState } from 'react'
// import WithAuth from 'features/auth/WithAuth'

import styles from './Profile.module.scss'
import ResultSummary from '../../../features/homepage/components/ResultSummary'
import TempHeader from '../../../features/homepage/components/Header'

const Profile: FC = () => {
  const [theme, setTheme] = useState('dark')

  const resultSummary = {
    title: 'Likely aphantastic',
    detail: 'Your ability to imagine sensory experiences - what you see, hear, feel, taste, smell - in your mind when there is no stimulus present is limited.'
  }

  return (
    <div id='profileContainer' className={`${styles.profileContainer} ${styles[theme]}`}>
      <TempHeader setTheme={setTheme} theme={theme} />
      <div className={styles.profileCoverWrapper}>
        <div className={styles.profileLeftWrapper}>
            This section is for Radar Chart
        </div>
        <ResultSummary resultSummary={resultSummary} />
      </div>
    </div>
  )
}


// export default WithAuth(Profile)
export default Profile
