export const homeContainer: string
export const light: string
export const dark: string
export const homeHeader: string
export const homeLeftWrapper: string
export const homeCoverWrapper: string
