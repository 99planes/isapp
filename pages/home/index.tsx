import React, { FC, useState } from 'react'
// import WithAuth from 'features/auth/WithAuth'
// import useAuth from 'features/auth'
// import { useRouter } from 'next/router'

import styles from './Home.module.scss'
import TempHeader from '../../features/homepage/components/Header'

const Home: FC = () => {
  const [theme, setTheme] = useState('dark')
  // const { loading, user } = useAuth()
  // const router = useRouter()

  // useEffect(() => {
  //   if (!loading && !user) {
  //     router.replace('/login')
  //   }
  // }, [user, loading, router])


  return (
    <div id='homeContainer' className={`${styles.homeContainer} ${styles[theme]}`}>
      <TempHeader setTheme={setTheme} theme={theme} />
      <div className={styles.homeCoverWrapper}>
        This is Home page
      </div>
    </div>
  )
}


// export default WithAuth(Home)
export default Home
